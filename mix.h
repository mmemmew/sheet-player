#ifndef MIX_H
#define MIX_H
#include "util.h"

U_ATTR Pulse mix_sound(Wave w, LENT n);

#endif
