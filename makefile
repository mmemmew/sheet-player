CC=gcc
CPPFLAGS= -Wall -c # -I
LIBS= -Wall -O2 -L /usr/local/lib -lavutil -lavcodec -lavformat # -L /Users/durand/Desktop/Centre/A\ propos\ de\ programmes/C/player/lib -lglfw3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -L /usr/local/lib -lavutil -lswscale -lavcodec
out=output.mp3
SOURCES=main.c instrument.c util.c parser.c mix.c

all: main TAGS

%.d: %.c
	@echo "Remaking makefiles..."
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$
	@echo "Done"

include $(SOURCES:.c=.d)

TAGS: *.c *.h
	etags $+

# parser.o : parser.c parser.h util.h instrument.h mix.h
# 	$(CC) $(CPPFLAGS) $< -o $@

main: $(SOURCES:.c=.o)
	$(CC) $(LIBS) -o $@ $+

.PHONY: clean all play clear cat tur coffin

clean:
	@echo "Cleaning..."
	-@rm -rf *.o main TAGS *.dYSM 2>/dev/null || true
	@echo "Done."

$(out): main
	./main

# play: $(out)
# 	ffplay -f f32le -ar 44100 -showmode 1 -autoexit output.mkv

play: $(out)
	mpv $<

tur: main
	@echo "making turkish march..."
	-@rm notes.txt
	@ln -s turkish/turkish\ notes.txt notes.txt
	@./main
	@echo "Done."

marble: main
	@echo "making marble machine..."
	-@rm notes.txt
	@ln -s marble\ machine/marble\ machine\ notes.txt notes.txt
	@./main
	@echo "Done."

cat: main
	@echo "making cat vibing..."
	-@rm notes.txt
	@ln -s cat/part\ of\ cat\ vibing.txt notes.txt
	@./main
	@echo "Done."

coffin: main
	@echo "making coffin dance..."
	-@rm notes.txt
	@ln -s coffin/part\ of\ coffin\ dance.txt notes.txt
	@./main
	@echo "Done."

elise: main
	@echo "making Für Elise..."
	-@rm notes.txt
	@ln -s "Für Elise/Für Elise notes.txt" notes.txt
	@./main
	@echo "Done."

single: main
	@echo "making Single Notes..."
	-@rm notes.txt
	@ln -s "test.txt" notes.txt
	@./main
	@echo "Done."


clear:
	@echo "Deleting $(out)..."
	-@rm $(out)
	@echo "Done."

test-parser.o: parser.c parser.h util.h
	$(CC) -Wall -c -DTEST -o $@ $<

test: util.o test-parser.o instrument.o
	$(CC) -Wall -O2 -o $@ $+
