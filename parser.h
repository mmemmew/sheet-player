#ifndef PARSER_H
#define PARSER_H
#include "util.h"
#include "instrument.h"

/* Parse a complex sheet file so that it is easy to generate complex
   sounds. */

/* A unit for the parser.  This typically represents one note, but can
   serve different purposes.  In our format, a unit is given in one
   line. */

struct PUnit_s;

typedef struct PUnit_s PUnit;

struct PNote_s;

typedef struct PNote_s PNote;

/* A sheet is just an array of units.  But we need to know the length
   of the array as well. */

typedef struct {
  PUnit *data;
  LENT len;
} PSheet;

U_ATTR void print_sheet(const PSheet sh);

/* Might be helpful, or not */

H_ATTR unsigned char is_empty_note(PNote *n);

/* The function to read the sheet. */

U_ATTR PSheet read_sheet(const char *str, LENT len);

/* Destructor */

U_ATTR void destroy_sheet(PSheet sh);

/* Now play sheet */

U_ATTR WaveFrag play_sheet(const PSheet sh, const Volume v);

#endif

