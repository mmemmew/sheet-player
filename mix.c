#include "mix.h"
#include <stdio.h>
#include <stdlib.h>

/* Mix different sounds together.  For now this is just a simple
   averaging function.  But more sophostigated mixing techniques such
   as filters and reverbs might get includede in the future. */

U_ATTR
Pulse
mix_sound(Wave w, LENT n)
{
  Pulse result = 0.0;

  for (LENT i = 0; i < n;)
    result += *(w+i++);

  result /= (NUMT) n;

  return result;
}
