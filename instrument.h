#ifndef INSTRUMENT_H
#define INSTRUMENT_H
#include "util.h"

struct Instrument_s;

typedef struct Instrument_s Instrument;

typedef WaveFrag (*sound_t)(void *, Volume v, Hertz h, Seconds s);
typedef WaveFrag (*merger_t)(void *, WaveFrag *, LENT, int);

WaveFrag make_sound(Instrument * in, Volume v, Hertz h, Seconds s);

WaveFrag piano_sound(void * in, Volume v, Hertz h, Seconds s);

/* Need this so that we can let instruments control reverberation,
   say. */

U_ATTR WaveFrag merge_waves(Instrument *in, WaveFrag *frags, LENT len, int step);

/* Various instruments */

Instrument *make_piano();

void destroy_piano(Instrument *in);

/* Violin */

Instrument * make_violin();

void destroy_violin(Instrument *in);

#endif
